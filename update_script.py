import search
import time
import traceback

while True:
    try:
        search.populate_latest("/longform/movie/","wO71mdeXPuWAgYISZbwnsusHTzo1f1ttpDsWZHU7eoTL-eps9zdpEw..")
        search.populate_latest("/shortform/movie/","v6iySps7-vgL_wPxQQ9Q1ZuvBtNlRKzsN32fGUQ5rf1fB0U7WhUmfw..")
    except:
        print "Error populating"
        traceback.print_exc()
    time.sleep(60)   
    