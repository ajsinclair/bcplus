from flask import Flask, request, render_template
import flask_cors
#from search import search as esearch
#from search import get_asset, search_series, more_like_this, exact_match
import search as essearch
import json
import os
import time
import brightcove as brightcove
import sys
from crossdomain import crossdomain
import xml.dom.minidom
import requests
import logging

sys.path.insert(0, '/Users/andrew/Desktop/workspace/AdaptivePlayout/adstitch')
import freewheel

logging.basicConfig(level=logging.DEBUG)
# create logger with 'spam_application'
logger = logging.getLogger(__name__)

'''
This is a test front end for a Brightcove based API
You can run it from the command line
'''

app = Flask(__name__)

@app.route('/favicon.ico')
@app.route('/favicon.ico/')
def favicon():
    return "OK"

@app.route('/search/<query>')
@app.route('/')
@app.route('/<catalog>/')
def search(query=None, catalog=None):
    if query is None:
        # the following is available to be used with the jQuery autocomplete tool 
        query = request.args.get("term",None)
    catalog = request.args.get("catalog",None)
    
    if catalog is None:
        results = essearch.search(query, index="/shortform,longform/movie/")
    else:
        results = essearch.search(query, index=os.path.join("/",catalog,"movie/"))
    response = []
    try:
        for item in results["hits"]["hits"]:
            response.append({
                             "type":item["_index"],
                             "label":item["_source"]["name"],
                             "id":item["_source"]["id"],
                             "value":item["_source"]["longDescription"],
                             "video":item["_source"]["FLVFullLength"]["url"],
                             "publishedDate": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item["_source"]["publishedDate"])/1000))
                             })
    except:
        #logger.error("Problem adding result")
        pass
    
    return json.dumps(response)

def get_msnvideo(id):
    # TODO no error handling!
    try:
        print "Query MSN with: " + str(id)
        resp = requests.get("http://edge3.catalog.video.msn.com/videoByCsid.aspx?csid=AU_ninemsn&providerid=" + str(id))
        myxml = xml.dom.minidom.parseString(resp.text)
        id = myxml.getElementsByTagName('uuid')
        return id[0].firstChild.nodeValue
    except:
        return None


@app.route('/bclatest')
def get_bclatest():
    data = brightcove.get_latest("v6iySps7-vgL_wPxQQ9Q1ZuvBtNlRKzsN32fGUQ5rf1fB0U7WhUmfw..",
                                 count=10, fields="video_fields=id%2Cname%2Clength%2CpublishedDate%2CreferenceId")
    result = []
    for item in data["items"]:
        ad_status, ad_error = freewheel.ad_request(item["id"])
        msnad_status, msnad_error = freewheel.ad_request(get_msnvideo(item["referenceId"]))
        pubdate = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(item["publishedDate"])/1000))
        result.append({
                       "brightcoveId":item["id"],
                       "title":item["name"],
                       "adstatus":ad_status,
                       "msnadstatus":msnad_status,
                       "aderror":ad_error,
                       "publishedDate":pubdate,
                       "stargateId":item["referenceId"]
                       })
    return json.dumps(result)

@app.route('/tpsearch/')
@app.route('/tpsearch/<term>')
def tpsearch(term=None):
    if term is None:
        # the following is available to be used with the jQuery autocomplete tool 
        term = request.args.get("term",None)
    results = essearch.search(term, index="/theplatform/movie/", sortfield="pubDate")
    
    response = []
    try:
        for item in results["hits"]["hits"]:
            try:
                response.append({
                                 "type":item["_index"],
                                 "label":item["_source"]["title"],
                                 "id":item["_source"]["guid"],
                                 "value":item["_source"]["description"],
                                 "video":item["_source"]["content"][0]["url"],
                                 "publishedDate": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item["_source"]["pubDate"])/1000))
                                })
                
            except:
                response.append("Error " + item)
    except:
        response.append("Error")
        #logger.error("Problem adding result")
        pass
    
    return json.dumps(response)

@app.route('/bc/asset/shortform/<id>')
def get_bc_video(id, apikey="v6iySps7-vgL_wPxQQ9Q1ZuvBtNlRKzsN32fGUQ5rf1fB0U7WhUmfw.."):
    data = brightcove.find_video_by_id(apikey, id)
    return data

@app.route('/bc/asset/longform/<id>')
def get_bc_video_longform(id, apikey="Ztcs2WuBL-Gmlf6j_ircdkgOqM9G_t1gNEoHrKpeO5v6kgHVH5M8RA.."):
    data = brightcove.find_video_by_id(apikey, id)
    return data

# TODO tidy up this pattern to use a dict of apikeys
@app.route('/bc/asset/devdrm/<id>')
def get_bc_video_devdrm(id, apikey="ZYCVrKskpTfa2kO-Cu4MadW8eMlkjpf_0xQrIRuunfsm_8jpywSNUg.."):
    data = brightcove.find_video_by_id(apikey, id)
    return data

@app.route('/search_page')
@app.route('/<catalog>/search_page')
def search_page(catalog=None):   
    return render_template('search_example.html',name=None, catalog=catalog)

@app.route('/tpsearch_page')
def tpsearch_page(catalog=None):  
    return render_template('tpsearch.html',name=None, catalog=catalog)

@app.route('/asset/<id>')
def asset(id):
    return essearch.get_asset(id)

@app.route('/asset/<catalog>/<id>')
def asset_shortform(id, catalog):
    return essearch.get_asset(id, "/" + catalog)

@app.route('/mlt/<id>')
@app.route('/mlt/<id>/<text>')
def mlt(id, text="any",catalog=None):
    return json.dumps(essearch.more_like_this(id, text))



@app.route('/series/<series_title>')
def series(series_title):
    term = {"series" : series_title}
    return json.dumps(essearch.exact_match(term, catalog="/longform/movie"))

@app.route('/seriesall')
def get_all_series():
    '''
    {
    "aggs" : { 
        "series" : { 
            "terms" : {
              "field" : "customFields.series" 
            }
        }
    }
}
    '''
    results = json.dumps(essearch.get_agg("series","customFields.series","/longform/movie"))
    return results

@app.route('/genres')
def get_all_genres():
    '''
    {
    "aggs" : { 
        "genre" : { 
            "terms" : {
              "field" : "customFields.genre" 
            }
        }
    }
}
    '''
    results = json.dumps(essearch.get_agg("genres","customFields.genre","/longform/movie"))
    return results

@app.route('/series/<series_title>/season/<season>')
def series_season(series_title, season, filter=None):
    term = [
            {
              "term": {
                "customFields.series": series_title
              }
            },
            {
              "term": {
                "customFields.season": season
              }
            }
          ]
    results = essearch.match_and(term, catalog="/longform/movie")
    if filter is None:
        return json.dumps(results)
    else:
        results_filtered = {
                            "hits": [
                                     {"hits":[]}
                                     ]
                            }
        for item in results["hits"]["hits"]:
            filtered_item = brightcove.object_filter(item["_source"],["name"])
            results_filtered["hits"]["hits"].append(results_filtered)
        return results_filtered
            

@app.route('/match/<term>/<value>')
def match(term, value):
    myterm = {term : value}
    return json.dumps(essearch.exact_match(myterm, value))
        
if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5009,debug=True)