import json

'''
Script to transform a JSON object from one format to another
Uses a basic template system
'''

source = {
          "id":12343434,
          "name":"This is the title of my object",
          "desc":"This is my description",
          "customFields": {
                            "genre": "Reality",
                            "series": "The Block",
                            "episode": "29",
                            "network": "Channel 9",
                            "season": "4"
                            },
          "thumbnail": {
                        "remoteUrl": None,
                        "referenceId": "88363e8b-d193-4848-b9fc-9bb33fa5d049_129842258261367808_Thumbnail",
                        "displayName": "88363e8b-d193-4848-b9fc-9bb33fa5d049.jpg",
                        "id": 1690455929001,
                        "type": "THUMBNAIL"
                        },
          "creationDate": "1339752544346"
          }

# Genre functions should go in a seperate class
def genre_exists(genre_name):
    # Faked for now
    # Should look up the genre in the index and return the ID
    return 1

def create_genre(genre_name):
    # Faked for now
    # Should create the genre and return the ID
    return 2

def get_genreid(genre_name):
    genre_result = genre_exists(genre_name)
    if genre_result is None:
        return create_genre(genre_name)
    else:
        return genre_result
        

destination = {
               "id":source["id"],
               "title":source["name"],
               "description":source["desc"],
               # Genre will need to do a look up to see if the genre already exists and if not create it
               "genre":get_genreid(source["customFields"]["genre"])
               
               }

print json.dumps(destination)