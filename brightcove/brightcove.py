'''
Copyright 2014 Andrew Sinclair

Available free to use no warranties
'''
import requests
import json
import logging
import hashlib
from ftplib import FTP
import traceback
import os

logger = logging.getLogger("bc_query")

WRITE_URL = "https://api.brightcove.com/services/post"
READ_URL = "http://api.brightcove.com/services/library"

FTP_SERVER = "upload.brightcove.com"


proxies = {
  #"http": "http://localhost:8888",
  #"https": "http://localhost:8888",
}

# These are used for the write operations
headers = {
               'content-type':'multipart/form-data; boundary=----WebKitFormBoundaryvkESDtgqn22F4Rei',
               'user-agent':"brightcove-api python"
               }

'''
Full list of BC Video fields available here
http://docs.brightcove.com/en/video-cloud/media/references/reference.html#Video
'''
#VIDEO_FIELDS = "&video_fields=id" + 

VIDEO_FIELDS="video_fields="\
"id%2C"\
"name%2C"\
"shortDescription%2C"\
"longDescription%2"\
"CcreationDate%2C"\
"publishedDate%2C"\
"lastModifiedDate%2C"\
"endDate%2C"\
"startDate%2C"\
"version%2C"\
"linkURL%2C"\
"linkText%2C"\
"tags%2C"\
"customFields%2C"\
"cuePoints%2C"\
"videoStillURL%2C"\
"videoStill%2C"\
"thumbnailURL%2C"\
"thumbnail%2C"\
"logoOverlay%2C"\
"referenceId%2C"\
"length%2C"\
"economics%2C"\
"playsTotal%2C"\
"playsTrailingWeek%2C"\
"FLVURL%2C"\
"renditions%2C"\
"iOSRenditions%2C"\
"HDSRenditions%2C"\
"hdsManifestUrl%2C"\
"dashManifestUrl%2C"\
"WVMRenditions%2C"\
"smoothRenditions%2C"\
"smoothManifestUrl%2C"\
"HLSURL%2C"\
"FLVFullLength%2C"\
"videoFullLength%2C"\
"captioning%2C"\
"adKeys%2C"\
"digitalMaster%2C"\
"logoOverlay%2C"\
"accountId%2C"\
"itemState%2C"\
"startDate%2C"\
"endDate%2C"\
"geoFiltered%2C"\
"geoFilteredCountries%2C"\
"geoFilterExclude%2C"\
"sharedByExternalAcct%2C"\
"sharedToExternalAcct"

###############################################

def get_latest(apikey, count=100, page=0, sort="PUBLISH_DATE%3ADESC", fields=VIDEO_FIELDS):
    '''
    Gets the top <count> videos ordered by date descending (newest first)
    '''
    
    url = READ_URL + "?command=search_videos\
&page_size=" + str(count) +  "&page_number=" + str(page) +\
"&media_delivery=http\
&sort_by=" + sort + "\
&get_item_count=true\
&token=" + apikey + "&" + fields
    
    print "url: " + url
    logger.debug(url)
    resp = requests.get(url)
    if resp.status_code == 200:
        data = json.loads(resp.text)
    else:
        data = None
        #logger.error("Problem getting data from Brightcove")
    
    return data

def get_latest_unfiltered(apikey, count=100, page=0, filter=None, fields=VIDEO_FIELDS):
    '''
    Gets the top <count> videos ordered by date descending (newest first)
    
    The filter command allows for the retrieval of videos with a specific state, possible values are:
    ACTIVE, INACTIVE, DELETED, PENDING
    To get active videos we use this query:
    &all=item_state:0,
    To get inactive videos we use this one:
    &all=item_state:1
    '''
    
    url = READ_URL + "?command=search_videos_unfiltered\
&page_size=" + str(count) +  "&page_number=" + str(page) +\
"&media_delivery=http\
&sort_by=PUBLISH_DATE%3ADESC\
&get_item_count=true\
&token=" + apikey + "&" + fields
    if filter is not None:
        url = url + "&filter=" + filter
    print url
    resp = requests.get(url)
    if resp.status_code == 200:
        data = json.loads(resp.text)
    else:
        data = None
        logger.error("Problem getting data from Brightcove")
    
    return data

def find_modified_videos(apikey):
    '''
    //api.brightcove.com/services/library?command=find_modified_videos&page_size=5
    &from_date=21492000&filter=PLAYABLE
    &video_fields=id%2Cname%2CshortDescription%2ClongDescription%2CcreationDate%2CpublishedDate
    %2ClastModifiedDate%2Cversion%2ClinkURL%2ClinkText%2Ctags%2CcustomFields%2CcuePoints
    %2CvideoStillURL%2CvideoStill%2CthumbnailURL%2Cthumbnail%2ClogoOverlay
    %2CreferenceId%2Clength%2Ceconomics%2CplaysTotal%2CplaysTrailingWeek%2CFLVURL
    %2Crenditions%2CiOSRenditions%2CHDSRenditions%2ChdsManifestUrl
    %2CWVMRenditions%2CsmoothRenditions%2CsmoothManifestUrl
    %2CHLSURL%2CFLVFullLength%2CvideoFullLength%2Ccaptioning%2CadKeys
    %2CdigitalMaster%2ClogoOverlay%2CaccountId%2CitemState
    %2CstartDate%2CendDate%2CgeoFiltered%2CgeoFilteredCountries
    %2CgeoFilterExclude%2CsharedByExternalAcct%2CsharedToExternalAcct
    &media_delivery=http&sort_by=PUBLISH_DATE&sort_order=ASC
    &page_number=0&get_item_count=true&callback=BCL.onSearchResponse&token=
    '''
    pass

def find_video_by_name(apikey, name):
    pass

def find_video_by_id(apikey, id, video_fields=VIDEO_FIELDS):
    url = "http://api.brightcove.com/services/library?command=find_video_by_id&video_id=" + id +"&"\
+ video_fields +\
"&media_delivery=http\
&token=" + apikey
    logger.debug(url)
    resp = requests.get(url)
    return json.dumps({"url": url,"response": json.loads(resp.text)})


def find_video_by_id_unfiltered(apikey, id, video_fields=VIDEO_FIELDS):
    url = "http://api.brightcove.com/services/library?command=find_video_by_id_unfiltered&video_id=" + id +"&"\
+ video_fields +\
"&media_delivery=http\
&sort_by=PUBLISH_DATE%3ADESC\
&page_number=0\
&get_item_count=true\
&token=" + apikey
    logger.debug(url)
    resp = requests.get(url)
    return resp.text

def find_video_by_refid(apikey, refid):
    url = "http://api.brightcove.com/services/library?command=search_videos&any=reference_id:"\
+ refid + "&"\
+ VIDEO_FIELDS +\
"&media_delivery=http\
&sort_by=PUBLISH_DATE%3ADESC\
&page_number=0\
&get_item_count=true\
&token=" + apikey
    #print url
    resp = requests.get(url)
    return resp.text


def object_filter(object, fields):
    '''
    Takes an object and only returns the required fields in the form
    fields = ["name","longDescription"]
    '''
    ret_object = {}
    for field in object:
        if field in fields:
            ret_object[field] = object[field]
        
    return ret_object

def do_post(create_json):
    post_data = "------WebKitFormBoundaryvkESDtgqn22F4Rei\n\
Content-Disposition: form-data; name='JSONView'\n\n" + create_json + "\n\n------WebKitFormBoundaryvkESDtgqn22F4Rei--"
    try:
        resp = requests.post(WRITE_URL, data=post_data, headers=headers, proxies=proxies)
    except:
        logging.error("Problem posting data")
        traceback.print_exc()
    return resp.text

def create_remote_asset(apikey, json_template):
    '''
    Uses a remote asset template file remote.json to create a new remote asset such as a live stream off Akamai
    You can also use this function with a file to update, see remote_update.json
    See here for docs: http://support.brightcove.com/en/video-cloud/docs/creating-videos-remote-assets-using-media-api
    '''
    create_json_file = open(json_template,'r')
    create_json = create_json_file.read()
    create_json = create_json.replace("<apikey>", apikey)
    #create_json = json.loads(create_json)
    
    post_data = "------WebKitFormBoundaryvkESDtgqn22F4Rei\n\
Content-Disposition: form-data; name='JSONView'\n\n" + create_json + "\n\n------WebKitFormBoundaryvkESDtgqn22F4Rei--"
    
    # Note for some unknown reason I was having issues with requests posting the multipart properly so have done it manually
    #files = {'file': ('report.csv', 'some,data,to,send\nanother,row,to,send\n'), 'json':json.dumps(create_json)}
    #print create_json
    resp = requests.post(WRITE_URL, data=post_data, headers=headers, proxies=proxies)
    # Response looks like this
    # {"result": {"id":3661526560001,"name":"9news.com.au livestream","adKeys":null,"shortDescription":"Watch live, streaming video from 9news.com.au","longDescription":null,"creationDate":"1404691138964","publishedDate":"1404691138964","lastModifiedDate":"1404697174498","linkURL":null,"linkText":null,"tags":[],"videoStillURL":"http:\/\/progressive.netshow.ninemsn.com.au\/media2\/664969388001\/2014\/07\/664969388001_3661688302001_9news-com-au-generic.jpg?pubId=664969388001","thumbnailURL":"http:\/\/progressive.netshow.ninemsn.com.au\/media2\/664969388001\/2014\/07\/664969388001_3661682249001_9news-com-au-generic-thumb.jpg?pubId=664969388001","referenceId":null,"length":-1,"economics":"FREE","playsTotal":null,"playsTrailingWeek":null}, "error": null, "id": null}
    return resp.text

def update_asset(apikey, id, video):
    '''
    Takes a BC apikey, an id of a video and a BC formated JSON video object with just the fields
    to be updated
    Note that only updated fields are posted
    '''
    
    post = {}
    post["method"] = "update_video"
    post["params"] = {}
    post["params"]["video"] = {}
    post["params"]["video"]["customFields"] = video
    post["params"]["video"]["id"] = id
    post["params"]["token"] = apikey
    
    post_data = "------WebKitFormBoundaryvkESDtgqn22F4Rei\n\
Content-Disposition: form-data; name='JSONView'\n\n" + json.dumps(post) + "\n\n------WebKitFormBoundaryvkESDtgqn22F4Rei--"
    resp = requests.post(WRITE_URL, post_data, proxies=proxies, headers=headers)
    
    return post_data + "\n" + resp.text

def md5_for_file(f, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.hexdigest()

def ftp_upload(filename, ftp_server, ftp_user, ftp_password, ftp_dir=".", refid=""):
    # Upload the file to the server
    logging.info("Sending %s to %s/%s" % (filename,ftp_server,ftp_dir))
    try:
        ftp = FTP(ftp_server, ftp_user, ftp_password)
        ftp.cwd(ftp_dir)
        f = open(filename, 'rb')
        stor_cmd = "STOR %s" % (refid + "_" + os.path.basename(filename))
        ftp.storbinary(stor_cmd,f)
        ftp.close()
        logging.info("Upload complete")
    except:
        logging.error("Problem uploading file %s via FTP" % filename)   
        traceback.print_exc() 
    pass

def create_asset_ftp(user, password, filename, video, pubid, email="noreply@mail.com", upload_xml="upload.xml"):
    '''
    Uploads via batch FTP to a Brightcove account with a source file and a video object
    Video object should be JSON formatted as per a normal BC video object and will be translated
    to BC FTP XML format, minimum required fields are
    video = {
        "referenceId":"ID",
        "name":"title of video",
        "shortDescription":"short description",
        "longDescription":"long description"
    }
    '''
    try:
        upload_xml_file = open(upload_xml,'r')
        upload_xml = upload_xml_file.read()
        upload_xml_file.close()
    except:
        logging.error("Problem opening upload XML template")
    
    # Open the file handle we can use for upload and generating the hash
    f = open(filename)
    
    #print video
    
    try:
        
        upload_xml = upload_xml.replace("<PUBID>", str(pubid))
        upload_xml = upload_xml.replace("<EMAIL>", email)
        upload_xml = upload_xml.replace("<MD5>", md5_for_file(f))
        upload_xml = upload_xml.replace("<FILENAME>", video["referenceId"] + "_" + os.path.basename(filename))
        upload_xml = upload_xml.replace("<REFID>", video["referenceId"])
        upload_xml = upload_xml.replace("<TITLE>", video["name"])
        upload_xml = upload_xml.replace("<TITLEREFID>", str(video["referenceId"]))
        upload_xml = upload_xml.replace("<SHORTDESCRIPTION>", video["shortDescription"])
        upload_xml = upload_xml.replace("<LONGDESCRIPTION>", video["longDescription"])
        upload_xml = upload_xml.replace("<PROFILE>", video["profile"])
        upload_xml = upload_xml.replace("<SIZE>", video["size"])
        upload_xml = upload_xml.replace("<ENDDATE>", "")
    except:
        logging.error("Problem mapping parameters in upload XML")
        traceback.print_exc()
    
    #print upload_xml
    upload_xml_file = "/tmp/upload."+ video["referenceId"] + ".xml"
    
    try:
        upload_xml_tmp = open(upload_xml_file,'w')
        upload_xml_tmp.write(upload_xml)
        upload_xml_tmp.close()
    except:
        traceback.print_exc()
    
    try:
        logging.info("Pushing %s to %s" % (filename, FTP_SERVER))
        ftp_upload(filename, FTP_SERVER, user, password, refid=video["referenceId"])
        logging.info("Pushing %s to %s" % (upload_xml_tmp, FTP_SERVER))
        ftp_upload(upload_xml_file, FTP_SERVER, user, password)
    except:
        traceback.print_exc()
    
    pass


    