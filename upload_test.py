'''
Script to test the upload and processing performance of Brightcove Video Clould

BC rule of thumb is 2x duration of video e.g. a 2 min video would be available after 4 mins from upload complete
'''
import brightcove
import uuid
import logging
import time
import json
import subprocess
from datetime import datetime
import os

FFPROBE = "/usr/local/bin/ffprobe"
user = "ninemsnftp-dev@mi9.com.au"
password = "ninemsn!23"

testfile = "/Users/andrew/Desktop/demo/hevc/dig_x265.mp4"
testresults = []
video_ids = []
UPDATE_EMAIL = "andrew.sinclair@mi9.com.au"

def get_duration(filename):
    cmd = FFPROBE + " -print_format json -show_format " + filename
    p = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    text = p.stdout.read()
    retcode = p.wait()
    return json.loads(text)["format"]["duration"]

def test_upload():
    video_id = str(uuid.uuid4())
    start_time = datetime.now()
    print "####Started: " + start_time.strftime("%H:%M:%S")
    
    video = {
            "referenceId":str(video_id),
            "name":"DASH low " + time.strftime("%H:%M:%S"),
            "shortDescription":"short description",
            "longDescription":"long description",
            "profile":"low",
            "size": str(os.path.getsize(testfile))
        }
    
    
    duration = float(get_duration(testfile))
    expected_duration = duration * 2
    print "Asset Duration: " + time.strftime("%H:%M:%S", time.gmtime(duration))
    #expected_finish = starttime + duration
    
    upload_start = datetime.now()
    print "Upload start: " + upload_start.strftime("%H:%M:%S")
    brightcove.create_asset_ftp(user, password, testfile, video, "2738061917001", email=UPDATE_EMAIL, upload_xml="/Users/andrew/Desktop/workspace/mi9/bcplus/jumpin_samples/brightcove.template.xml")
    print "Uploaded: " + time.strftime("%H:%M:%S")
    upload_complete = datetime.now()
    upload_time = upload_complete - upload_start
    print "Upload duration: " + time.strftime("%H:%M:%S", time.gmtime(upload_time.total_seconds()))
    
    '''
    Uncomment this to poll and check for the asset
    while True:
        resp = brightcove.find_video_by_refid("ZYCVrKskpTfa2kO-Cu4MadW8eMlkjpf_0xQrIRuunfsm_8jpywSNUg..",video_id)
        data = json.loads(resp)
        if len(data["items"]) == 0:
            #print "Waiting: " + time.strftime("%H:%M:%S")
            time.sleep(5)
        else:
            print "Complete: " + time.strftime("%H:%M:%S")
            complete_time = datetime.now()
            #print data["items"][0]
            break
    '''
    complete_time = datetime.now()
    
    print "#### SUMMARY ###"
    print "Started: " + start_time.strftime("%H:%M:%S")
    print "Finished: " + complete_time.strftime("%H:%M:%S")
    print "Expected duration: " + time.strftime("%H:%M:%S", time.gmtime(expected_duration))
    total_time = complete_time - start_time
    print "Actual duration: " + time.strftime("%H:%M:%S",time.gmtime(total_time.seconds))
    result = {
              "start":start_time.strftime("%H:%M:%S"),
              "finish":complete_time.strftime("%H:%M:%S"),
              "expected":time.strftime("%H:%M:%S", time.gmtime(expected_duration)),
              "actual":time.strftime("%H:%M:%S",time.gmtime(total_time.seconds))
              }
    testresults.append(result)


test_upload()
#for i in range(0,10):
#    test_upload()
    
#print testresults
