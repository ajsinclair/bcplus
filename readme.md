# Brightcove Plus (aka Brightcove like lightning) #

This library has been built to get around some of the limitations with the Brightcove libraries, 
most notably how fast they perform and how good the search capabilities, particularly for filtering against custom
like custom fields etc

You can also use this to search across multiple accounts

To set this up you first need ElasticSearch which is the core component of this see. http://elasticsearch.org

Simply download and run elasticsearch for your platform (if you can't follow the Elasticsearch instructions then my notes may be too hard for you...)

You will also need the Python elasticsearch module (noting that you will need python pip):

```
#!python

pip install elasticsearch
```


You then need to add your Brightcove API key to the search.py script
After that you will need to configure and populate your elasticsearch instance, in search.py there is a function called populate_all()
An easy way to run this is using Python on the command line within the directory where the files are located e.g.

```
#!python

>python
>import search
>search.populate_all()
```

This should initiate a pull of all your data from Brightcove and populate your ElasticSearch instance.

This also includes a basic Brightcove library for python that lives in the brightcove.py directory, not to use this you need to create a config.local file and
update your API key in here.