'''
This script contains a number of simple functions for interacting with an ElasticSearch server
It has been setup to work in the context of operation with a Brightcove based video backend
'''

import requests
import brightcove
import json
import elasticsearch.helpers
import ConfigParser
import os
import traceback
import logging

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))

logging.basicConfig(level=logging.DEBUG)
# create logger with 'spam_application'
logger = logging.getLogger(__name__ + "error")
logger.setLevel(logging.DEBUG)
infologger = logging.getLogger(__name__ + "info")
infologger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(os.path.join(CURRENT_DIR,'exception.log'))
fh.setLevel(logging.DEBUG)

infologfile = logging.FileHandler(os.path.join(CURRENT_DIR,'success.log'))
infologfile.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
infologfile.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
infologger.addHandler(infologfile)

config = ConfigParser.SafeConfigParser()
config.read(CURRENT_DIR + '/config.local')

SERVER = "http://localhost:9200"
PUT_MOVIE = "/movies/movie/"
PUT_SHORTFORM = "/shortform/movie/"

# Set this when you want to run a populate all

# Catchup DRM
# BC_API_KEY = "ogxhPgSphIVa2hhxbi9oqtYwtg032io4B4-ImwddYliFWHqS0UfMEw.."
# Dev DRM ZYCVrKskpTfa2kO-Cu4MadW8eMlkjpf_0xQrIRuunfsm_8jpywSNUg..
# Prod shortform 
BC_API_KEY = config.get("Brightcove", "apikey")

def add_to_index(catalog, id, item_data):
    try:
        resp = requests.post(SERVER + catalog + str(id), data=json.dumps(item_data))
        if resp.status_code == 200 or resp.status_code == 201:
            infologger.info("Added: " + str(id))
        else:
            logger.error("Error adding to index " + str(resp.status_code) + " " + resp.text)
    except:
        logger.error("Problem adding to index: " + str(id) + json.dumps(item_data))
    
def populate_all(catalog, apikey, items=10, start=0, unfiltered=False, filter=None):
    '''
    Use this function for a first run or to re-populate all records
    Catalog refers to where you want it to live in elasticsearch e.g. /movies/movie/
    
    Use the start option to set a start page, i.e. if you stopped at certain page before
    Use the unfiltered option to get all records even inactive and expired, note this needs a special api key
    '''
    print (__name__)
    if unfiltered:
        if filter:
            assets = brightcove.get_latest_unfiltered(apikey, count=items, page=start, filter=filter)
        else:
            assets = brightcove.get_latest_unfiltered(apikey, count=items, page=start)
    else:
        assets = brightcove.get_latest(apikey, count=items, page=start)
        
    infologger.debug("count: %s, page: %s, page size: %s" % (assets["total_count"], assets["page_number"], assets["page_size"]))
    
    if start > 0:
        total_pages = (assets["total_count"] / assets["page_size"]) - (start)
        print "Total pages: " + str(total_pages)
        start = start + 1
    else:
        total_pages = assets["total_count"] / assets["page_size"]
        
    for item in assets["items"]:
            #print item["id"]
            add_to_index(catalog, item["id"], item)
            #resp = requests.post(SERVER + catalog + str(item["id"]), data=json.dumps(item))
            #print resp.text
    
    if total_pages > 0:
        total_pages_mod = assets["total_count"] % assets["page_size"]
        logger.debug("count: %s, page: %s, page size: %s" % (assets["total_count"], assets["page_number"], assets["page_size"]))
        if total_pages_mod > 0: total_pages = total_pages + 1
        if start > 0:
            y = start
        else:
            y = 0
        for x in range(1, total_pages):
            # TODO monitor for errors on this
            try:    
                if unfiltered:
                    if filter:
                        assets = brightcove.get_latest_unfiltered(apikey, count=items, page=y, filter=filter)
                    else:
                        assets = brightcove.get_latest_unfiltered(apikey, count=items, page=y)
                else:
                    assets = brightcove.get_latest(apikey, count=items, page=y)
                    
                for item in assets["items"]:
                    add_to_index(catalog, item["id"], item)
                y = y + 1
            except KeyboardInterrupt:
                break
            except:
                logger.error("Error processing request to Brightcove")
                traceback.print_exc()
                #print resp.status_code
                 # "Error: " + str(assets)
                # Retry in case of error
                
def compare_assets(catalog, assets, field):
    '''
    Compare an asset in the form of a dict with a field extract from local search DB
    the results are logged
    '''
    for item in assets["items"]:
        print item["id"]
        resp = requests.get(SERVER + catalog + str(item["id"]))
        local_asset = json.loads(resp.text)["_source"]
        # Just check the customFields as the rest will be a jumble of JSON/Dict
        if resp.status_code == 200 or resp.status_code == 201:
            if item[field] == local_asset[field]:
                print "Match"
                infologger.debug("Match: " + str(item["id"]))
            else:
                print "Mismatch"
                # Write out the mismatches for further analysis
                logger.debug("Mismatch: " + str(item["id"]))
                f = open('exceptions/' + str(item["id"]) + "_" + field + "_remote.json",'w')
                f.write(json.dumps(item["customFields"], indent=4, sort_keys=True))
                f.close()
                f = open('exceptions/' + str(item["id"]) + "_" + field + "_local.json",'w')
                f.write(json.dumps(local_asset["customFields"], indent=4, sort_keys=True))
                f.close()
        else:
                print "Error getting asset from local index " + str(resp.status_code) + " " + resp.text
    return True
    
def get_assets(apikey, count, page, filter, unfiltered):
    if unfiltered:
        if filter:
            return brightcove.get_latest_unfiltered(apikey, count=count, page=page, filter=filter)
        else:
            return brightcove.get_latest_unfiltered(apikey, count=count, page=page)
    else:
        return brightcove.get_latest(apikey, count=items, page=page)

def compare_all(catalog, apikey, items=10, start=0, unfiltered=False, filter=None):
        
    assets = get_assets(apikey, items, start, filter, unfiltered)
        
    if start > 0:
        total_pages = (assets["total_count"] / assets["page_size"]) - (start)
        print "Total pages: " + str(total_pages)
        start = start + 1
    else:
        total_pages = assets["total_count"] / assets["page_size"]
        
    infologger.debug("count: %s, page: %s, page size: %s" % (assets["total_count"], assets["page_number"],
                                                           assets["page_size"]))
    
    compare_assets(catalog, assets, "customFields")
    
    if total_pages > 0:
        total_pages_mod = assets["total_count"] % assets["page_size"]
        if total_pages_mod > 0: total_pages = total_pages + 1
        if start > 0:
            y = start
        else:
            y = 0
        for x in range(1, total_pages):
            # TODO monitor for errors on this
            try:
                assets = get_assets(apikey, items, y, filter, unfiltered)                  
                compare_assets(catalog, assets, "customFields")                   
                    # print resp.text
                y = y + 1
            except KeyboardInterrupt:
                break
            except:
                print "Error processing resp"
                traceback.print_exc()
                #print resp.status_code

def update_bcassets(catalog, assets, field, apikey):
    '''
    Takes a list of assets and updates the assets in BC using the values from the local search index
    '''
    for item in assets["items"]:
        try:
            url = SERVER + catalog + str(item["id"])
            resp = requests.get(url)
            update_data = json.loads(resp.text)["_source"][field]
            print update_data
            # TODO remove hardcoded write apikey
            brightcove.update_asset("ZYCVrKskpTfa2kO-Cu4MadW8eMlkjpf_zZXAMp6KHEBn0ewScDxDrA..", item["id"], update_data)
        except:
            logger.error("Problem getting local asset for ID: " + str(item["id"]))
       
def update_all(catalog, apikey, items=10, start=0, unfiltered=False, filter=None, field="customFields"):
    '''
    Updates the remote catalog with a field from the local catalog
    '''
    assets = get_assets(apikey, items, start, filter, unfiltered)
        
    if start > 0:
        total_pages = (assets["total_count"] / assets["page_size"]) - (start)
        print "Total pages: " + str(total_pages)
        start = start + 1
    else:
        total_pages = assets["total_count"] / assets["page_size"]
        
    infologger.debug("count: %s, page: %s, page size: %s" % (assets["total_count"], assets["page_number"],
                                                           assets["page_size"]))
    
    update_bcassets(catalog, assets, field, None)
    
    if total_pages > 0:
        total_pages_mod = assets["total_count"] % assets["page_size"]
        if total_pages_mod > 0: total_pages = total_pages + 1
        if start > 0:
            y = start
        else:
            y = 0
        for x in range(1, total_pages):
            # TODO monitor for errors on this
            try:
                assets = get_assets(apikey, items, y, filter, unfiltered)                  
                update_bcassets(catalog, assets, field, None)                  
                    # print resp.text
                y = y + 1
            except KeyboardInterrupt:
                break
            except:
                print "Error processing resp"
                traceback.print_exc()
                print resp.status_code
        
def get_asset(id, catalog="/movies"):    
    resp = requests.get(SERVER + catalog + "/movie/" + id)
    return resp.text

def populate_latest(catalog, apikey, items=100):
    '''
    Update the latest items, default=100 of the latest items
    catalog refers to the catalog and asset type in elasticsearch e.g. /shortform/movie/
    '''
    assets = brightcove.get_latest(apikey, count=items)
    for item in assets["items"]:
        # print item
        resp = requests.put(SERVER + catalog + str(item["id"]), data=json.dumps(item))
        print resp.text
        
def search(term, start=0, size=10, index=None, sortfield="publishedDate", sortorder="desc"):
    '''
    Query against the search ID
    Default is to return the first 10 results and sort by the latest publish date
    indices is an array that allows for querying 1 or more specific indices
    Example usage: search("*",start=0,size=4000,index="/longform/")
    '''
    query = {
             "from" : start, "size" : size,
             "query": {
                       "query_string": {
                                        "query": term
                                        }
                       },
             "sort": { sortfield: { "order": sortorder }}
             }
   
    if index:
        url = SERVER + index + "/_search"
    else:
        url = SERVER + "/_search"
        
    resp = requests.post(url, data=json.dumps(query))
    return json.loads(resp.text)   

def search_series(term, start=0, size=100, catalog="/longform/movies"):
    query = {
             "from" : start, "size" : size,
             "fields" : ["series"],
             "query" : {
                        "query_string" : 
                            {  "query" : term }
                        }
             }
    resp = requests.post(SERVER + catalog + "/_search", data=json.dumps(query))
    return json.loads(resp.text) 

def exact_match(term, catalog="/longform/movie"):
    '''
    Does an exact match using specific terms e.g.
    "term": {
          "customFields.series": "The Block",
          "customFields.season": "glasshouse"
        }
    '''
    query = {
             "query" : {
                       "filtered" : { 
                                     "query" : {
                                                "match_all" : {}
                                                },
                                     "filter" : {
                                                 "term" : term
                                                 }
                                     }
                       }
             }
    logging.debug("Query: " + json.dumps(query))
    url = SERVER + catalog + "/_search" 
    logging.debug(url)
    logging.debug(json.dumps(query))
    resp = requests.post(url, data=json.dumps(query))
    return json.loads(resp.text)

def match_and(terms, catalog):
    '''
    Take an array of params to match e.g. a series and a season
    Can also be used for generic all AND matches
    [
        {"season","The Block"},
        {"series","glasshouse"}
    ]
    
    '''
        
    query = {
             "query": {
                "filtered": {
                  "query": {
                    "match_all": {}
                  },
                  "filter": {
                    "bool": {
                      "must": terms
                    }
                  }
                }
              }
             }
    
    logging.debug("Query: " + json.dumps(query))
    url = SERVER + catalog + "/_search" 
    logging.debug(url)
    logging.debug(json.dumps(query))
    resp = requests.post(url, data=json.dumps(query))
    return json.loads(resp.text)

def get_agg(agg, field, catalog):
    '''
    Queries a field and returns an array of unique values
    '''
    query = {
             "aggs" : {
                    agg : {
                            "terms" : { 
                                       "field" : field
                                      }
                            }
                        }
             }

    logging.debug(json.dumps(query))
    resp = requests.post(SERVER + catalog + "/_search?search_type=count", data=json.dumps(query))
    data = json.loads(resp.text)
        
    return data["aggregations"]

def create_mapping(index, mapping_file):
    '''
    Takes an index and a file and creates a new index with mapping
    '''
    f = open(mapping_file,'r')
    mapping = f.read()
    f.close()
    print mapping
    resp = requests.put(SERVER + index, data=mapping)
    
    return resp.text

def update_mapping(index, catalog_type, field):
    '''
    Use this to update the mapping for a field in a catalog
    
    
    '''
    query = {
             "properties": {
                            field: {
                                    "type":      "string",
                                    "analyzer":  "standard"
                                     }
                            }
             }
    resp = requests.put(SERVER + index + "/_mapping/" + catalog_type, data=json.dumps(query))
    print resp.text

def delete_index(index):
    resp = requests.delete(SERVER + "/" + index)
    print resp.text

def reindex():
    elasticsearch.helpers.reindex(es, "movies", "newmovies")
    
def more_like_this(id, text, catalog="/shortform/movie/", sortfield="publishedDate", sortorder="desc"):
    '''
    Uses the ES More Like This query to extract items with similar keywords
    Defaults to shortform catalog for the moment
    '''
    query = {
             "query": {
                 "more_like_this" : 
                 {
                    "fields" : ["name", "shortDescription", "longDescription", "tags", "customFields", "linkURL"],
                    "like_text" : text,
                    "min_term_freq" : 1,
                    "max_query_terms" : 10
                    }
                 },
             "sort": { sortfield: { "order": sortorder }}
             }
    resp = requests.post(SERVER + catalog + id + "/_mlt", data=json.dumps(query))
    data = json.loads(resp.text)
    result = []
    print data
    for item in data["hits"]["hits"]:
        # print item
        result.append(
                      {"id" : item["_source"]["id"],
                       "name" : item["_source"]["name"],
                       "link" : "http://localhost:5009/asset/shortform/" + str(item["_source"]["id"]),
                       "thumbnail" : item["_source"]["thumbnailURL"],
                       "type" : "shortform"
                       })
    return result

def count(catalog):
    url = SERVER + catalog + "/_count"
    count = 0
    try:
        resp = requests.get(url)
        data = json.loads(resp.text)
        count = data["count"]
    except:
        print "Problem getting count"
        
        
    return count






    
    
