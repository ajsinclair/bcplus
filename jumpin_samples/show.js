{
    "slug" : "{{slug}}",
    "secondaryIds" : {
    "servicesFrameworkSectionId" : "{{servicesFrameworkSectionId}}",
    "hwwProgramId" : "{{hwwProgramId}}"
    },
    "title" : "{{tile}}",
    "primaryColour" : "{{primaryColour}}",
    "tvChannel" : "{{tvChannel}}",
    "description" : "{{description}}",
    "episodeCount" : {{episodeCount}},
    "drm" : {{drm}},
    "country" : "{{country}}",
    "genre" : "{{genre}}",
    "language" : "{{language}}",
    "image" : {
    "showImage" : "{{showImage}}"
    },
    "nextEpisode" : {{nextEpisode}},
    "seasons" : [{
    "slug" : "show/aflfootyshow/season/2014",
    "secondaryIds" : {
    "servicesFrameworkId" : "7810757"
    },
    "title" : "2014",
    "description" : "",
    "numberEpisodes" : 2,
    "show" : {
    "slug" : "show/aflfootyshow",
    "_links" : {
    "self" : "http://tv-api-cat.api.jump-in.com.au/shows/show%2Faflfootyshow"
    }
    },
    "episodes" : [{
    "slug" : "show/aflfootyshow/season/2014/episode/18",
    "_links" : {
    "self" : "http://tv-api-cat.api.jump-in.com.au/episodes/show%2Faflfootyshow%2Fseason%2F2014%2Fepisode%2F18"
    }
    }, {
    "slug" : "show/aflfootyshow/season/2014/episode/17",
    "_links" : {
    "self" : "http://tv-api-cat.api.jump-in.com.au/episodes/show%2Faflfootyshow%2Fseason%2F2014%2Fepisode%2F17"
    }
    }],
    "airDate" : 1403780400000,
    "_links" : {
    "self" : "http://tv-api-cat.api.jump-in.com.au/seasons/show%2Faflfootyshow%2Fseason%2F2014"
    }
    }],
    "_links" : {
    "self" : "http://tv-api-cat.api.jump-in.com.au/shows/show%2Faflfootyshow"
}
