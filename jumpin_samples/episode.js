{
	"showTitle": "AFL Footy Show",
	"seasonTitle": "2014",
	"title": "Episode 18",
	"slug": "show/aflfootyshow/season/2014/episode/18",
	"secondaryIds": {
		"servicesFrameworkId": "8869800"
	},
	"description": "Garry Lyon, James Brayshaw, Sam Newman, Billy Brownless and Shane Crawford bring viewers all the latest in the world of AFL. Plus, favourite segments Street Talk and Sam's Mailbag.",
	"active": true,
	"airDate": 1404383400000,
	"tvChannel": "Channel 9",
	"images": {
		"videoStill": "http://progressive.netshow.ninemsn.com.au/media2/1622448031001/2014/07/1622448031001_3658015038001_140703-AFL.jpg?pubId=1622448031001"
	},
	"genre": "Sport",
	"videoExpiryDate": 1405611000000,
	"videoAvailableDate": 1404387000000,
	"videoId": "3656916845001",
	"season": {
		"slug": "show/aflfootyshow/season/2014",
		"_links": {
			"self": "http://tv-api-cat.api.jump-in.com.au/seasons/show%2Faflfootyshow%2Fseason%2F2014"
		},
		"secondaryIds": {
			"servicesFrameworkId": "7810757"
		},
		"title": "2014",
		"description": "",
		"numberEpisodes": 2,
		"show": {
			"slug": "show/aflfootyshow",
			"_links": {
				"self": "http://tv-api-cat.api.jump-in.com.au/shows/show%2Faflfootyshow"
			}
		},
		"episodes": [
			{
				"slug": "show/aflfootyshow/season/2014/episode/18",
				"_links": {
					"self": "http://tv-api-cat.api.jump-in.com.au/episodes/show%2Faflfootyshow%2Fseason%2F2014%2Fepisode%2F18"
				}
			}, {
				"slug": "show/aflfootyshow/season/2014/episode/17",
				"_links": {
					"self": "http://tv-api-cat.api.jump-in.com.au/episodes/show%2Faflfootyshow%2Fseason%2F2014%2Fepisode%2F17"
				}
			}
		],
		"airDate": 1403780400000
	},
	"show": {
		"slug": "show/aflfootyshow",
		"_links": {
			"self": "http://tv-api-cat.api.jump-in.com.au/shows/show%2Faflfootyshow"
		},
		"secondaryIds": {
			"servicesFrameworkSectionId": "6680481",
			"hwwProgramId": "165723"
		},
		"title": "AFL Footy Show",
		"primaryColour": "#df0000",
		"tvChannel": "Channel 9",
		"description": "The Logie award-winning AFL Footy Show celebrates its 20th season, starring Garry Lyon, James Brayshaw, Sam Newman, Billy Brownless and Shane Crawford. The boys will bring viewers all the latest in the world of AFL including, breaking news, team line-ups and entertainment. Plus, favourite segments Street Talk and Sam's Mailbag.",
		"episodeCount": 2,
		"drm": false,
		"country": "AUS",
		"genre": "Sport",
		"language": "English",
		"image": {
			"showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/AFL_FootyShow1280.jpg"
		},
		"nextEpisode": null,
		"seasons": [
			{
				"slug": "show/aflfootyshow/season/2014",
				"_links": {
					"self": "http://tv-api-cat.api.jump-in.com.au/seasons/show%2Faflfootyshow%2Fseason%2F2014"
				}
			}
		]
	},
	"episodeNumber": "18",
	"durationSeconds": 5814,
	"classification": "M",
	"_links": {
		"self": "http://tv-api-cat.api.jump-in.com.au/episodes/show%2Faflfootyshow%2Fseason%2F2014%2Fepisode%2F18"
	}
}