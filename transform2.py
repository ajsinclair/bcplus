from jinja2 import Environment, PackageLoader


env = Environment(loader=PackageLoader('jstransform', 'jumpin_samples'))


template = env.get_template("show.js")
print template.render(slug="myshow")

