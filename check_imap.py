#!/usr/bin/env python
import imaplib,re
from email.parser import HeaderParser

def gmail_checker(username,password):
        
        i=imaplib.IMAP4_SSL('imap.gmail.com')
        try:
                i.login(username,password)
                x,y=i.status('INBOX','(MESSAGES UNSEEN)')
                messages=int(re.search('MESSAGES\s+(\d+)',y[0]).group(1))
                unseen=int(re.search('UNSEEN\s+(\d+)',y[0]).group(1))
                for item in y:
                    print dir(i)
                return (messages,unseen)
        except:
                return False,0

def get_headers(username,password):
# Use in your scripts as follows:
    conn = imaplib.IMAP4_SSL('imap.gmail.com')
    conn.login(username, password)
    conn.select()
    conn.search(None, 'ALL') # returns a nice list of messages...
                             # let's say I pick #1 from this
    for i in range(0,5):
        try:
            data = conn.fetch(i, '(BODY[HEADER])')
            #for item in data:
            #    print item
            # gloss over data structure of return... I assume you know these
            # gives something like:
            # ('OK', [(1 (BODY[HEADER] {1662', 'Received: etc....')])
            header_data = data[1][0][1]
    
            parser = HeaderParser()
            msg = parser.parsestr(header_data)
            print msg["From"], msg["Subject"]
            print msg.keys()
        except:
            pass

get_headers('mscreen.testing@gmail.com','Smt2013!')
#messages,unseen = gmail_checker('mscreen.testing@gmail.com','Smt2013!')
#print "%i messages, %i unseen" % (messages,unseen)